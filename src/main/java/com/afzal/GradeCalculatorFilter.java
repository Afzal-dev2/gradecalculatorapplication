package com.afzal;

import java.io.IOException;
import java.text.DecimalFormat;

import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebFilter;


@WebFilter("/GradeCalculatorServlet")
public class GradeCalculatorFilter implements Filter {

    public void init(FilterConfig fConfig) throws ServletException {
        // Initialization code goes here
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        // Retrieve subject marks from the request
        int mark1 = Integer.parseInt(request.getParameter("subject1"));
        int mark2 = Integer.parseInt(request.getParameter("subject2"));
        int mark3 = Integer.parseInt(request.getParameter("subject3"));

        // Calculate the average
        double average = (mark1 + mark2 + mark3) / 3.0;
        
        DecimalFormat df = new DecimalFormat("#.##");
        average = Double.parseDouble(df.format(average));

        // Assign grade based on average
        String grade;
        if (average >= 90) {
            grade = "A Grade";
        } else if (average >= 80 && average <= 89) {
            grade = "B Grade";
        } else if (average >= 70 && average <= 79) {
            grade = "C Grade";
        } else if (average >= 60 && average <= 69) {
            grade = "D Grade";
        } else if (average >= 50 && average <= 59) {
            grade = "E Grade";
        } else {
            grade = "Error: Average less than 50";
        }

        // Set grade and average as request attributes
        request.setAttribute("average", average);
        request.setAttribute("grade", grade);

        // Forward the request to the servlet
        chain.doFilter(request, response);
    }

    public void destroy() {
        // Cleanup code
    }
}
