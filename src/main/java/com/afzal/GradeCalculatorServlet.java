package com.afzal;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

@WebServlet("/GradeCalculatorServlet")
public class GradeCalculatorServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        // Retrieve grade and average from request attributes
        String grade = (String) request.getAttribute("grade");
        double average = (double) request.getAttribute("average");

        // Sending the response back to the client
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h2>Grade Calculation Result</h2>");
        out.println("<p>Average Marks: " + average + "</p>");
        out.println("<p>Grade: " + grade + "</p>");
        out.println("</body></html>");
    }
}
