# Grade Calculator Application

This application calculates the grade based on the marks entered by the user for three subjects. It consists of an HTML form, JavaScript code, a filter class, and a servlet class.

## Table of Contents

- [Prerequisites](#prerequisites)
- [Usage Steps](#usage-steps)
- [Front-end (index.html)](#front-end-indexhtml)
- [JavaScript](#javascript)
- [Backend - Filter](#backend---filter)
  - [Filter Class (GradeCalculatorFilter.java)](#filter-class-gradecalculatorfilterjava)
- [Servlet](#servlet)
  - [Servlet Class (GradeCalculatorServlet.java)](#servlet-class-gradecalculatorservletjava)
- [Screenshots](#screenshots)

## Prerequisites

To run this application, you need the following:

1. Eclipse IDE with Tomcat server installed.
2. Java Development Kit (JDK) installed.

## Usage Steps

1. Open Eclipse IDE.
2. Import the project into Eclipse by selecting "File" > "Import" > "Existing Projects into Workspace" and browse to the location of the project.
3. Run the project in Tomcat server.
4. Open a web browser and enter the URL: http://localhost:8080/GradeCalculatorApplication/

## Front-end (index.html)

1. Contains an HTML form with input fields for three subject marks.
2. Includes a button to calculate the grade.
3. Decorated with CSS.

## JavaScript (defined under script tag in index.html file)

1. Defines a function `calculateGrade()` to handle form submission.
2. Collects the subject marks entered by the user.
3. Sends an AJAX request to the servlet with the marks data.
4. Receives the response from the servlet and displays the result on the webpage.

## Backend - Filter

### Filter Class (GradeCalculatorFilter.java)

1. Implements the Filter interface to intercept incoming requests.
2. Retrieves the subject marks from the request parameters.
3. Calculates the average of the marks and assigns the grade.
4. Sets the grade and average as request attributes.
5. Forwards the request to the servlet that displays the result.

## Servlet

### Servlet Class (GradeCalculatorServlet.java)

1. Receives POST requests from the frontend.
2. Retrieves the grade and average from request attributes set by the filter.
3. Optionally, limits the average to two decimal places.
4. Constructs an HTML response containing the average and grade.
5. Sends the response back to the client.

## Screenshots

![Screenshot1](/screenshots/screenshot1.png)
![Screenshot2](/screenshots/screenshot2.png)
![Screenshot3](/screenshots/screenshot3.png)
![Screenshot4](/screenshots/screenshot4.png)
![Screenshot5](/screenshots/screenshot5.png)
![Screenshot6](/screenshots/screenshot6.png)
![Screenshot7](/screenshots/screenshot7.png)
